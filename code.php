<?php

class Person{
	public $firstName;
	public $middleName;
	public $lastName;

	public function __construct($firstName, $middleName, $lastName){
		$this->firstName = $firstName;
		$this->middleName = $middleName;
		$this->lastName = $lastName;
	}

	public function printName(){
		return "Your full name is $this->firstName $this->middleName $this->lastName.";
	}
}

$person = new Person("Senku","","Ishigami");

class Developer extends Person{
	public function printName(){
		return "Your name is $this->firstName $this->middleName $this->lastName and you are a developer.";
	}

	public function getName(){
		$this->firstName;
		$this->middleName;
		$this->lastName;
	}

	public function setName($name){
		$this->firstName = $firstName;
		$this->middleName = $middleName;
		$this->lastName = $lastName;
	}
}

$developer = new Developer("John","Finch","Smith");

class Engineer extends Person{
	public function printName(){
		return "You are an engineer named $this->firstName $this->middleName $this->lastName.";
	}

	public function getName(){
		$this->firstName;
		$this->middleName;
		$this->lastName;
	}

	public function setName($name){
		$this->firstName = $firstName;
		$this->middleName = $middleName;
		$this->lastName = $lastName;
	}
}

$engineer = new Developer("Harold","Myers","Reese");

?>