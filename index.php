<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>PHP-KT-Activity 3</title>
</head>
<body style="padding: 10%;">
	<div style="border: 1px solid black; padding: 5%; margin: 10%;">
		<h1>Person</h1>
		<p><?= $person->printName();?></p>
		<h1>Developer</h1>
		<p><?= $developer->printName();?></p>
		<h1>Engineer</h1>
		<p><?= $engineer->printName();?></p>
	</div>
	
</body>
</html>